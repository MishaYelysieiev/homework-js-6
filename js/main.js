createUser = () => {
    let newUser = {};
    
    Object.defineProperties(newUser, {
        'firstName': {
            value: prompt("Enter your first name"," "),
            writable:false,
            configurable:true,
        },
        'lastName': {
            value: prompt("Enter your last name"," "),
            writable:false,
            configurable:true,
        },
        'getLogin': {
            get: function() {
            return newUser.firstName[0].toLowerCase()+newUser.lastName.toLowerCase()}
        },
        'setFirstName': {
           set: function(value) { Object.defineProperty(this, 'firstName', {value: value})}
        },
        'setLastName': {
            set: function(value) { Object.defineProperty(this, 'lastName', {value: value})}
        }
    });
    return newUser;
}

const user1 = createUser();

console.log(user1);

console.log(user1.getLogin);

user1.getLogin;
user1.setFirstName = "Jim"; ////does work////
user1.setLastName = "Parsons"; ////does work////
user1.firstName = "changed"; ////doesn't work////
user1.lastName = "changed";  ////doesn't work////

